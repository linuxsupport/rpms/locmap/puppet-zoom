Name:           puppet-zoom
Version:        1.4
Release:        1%{?dist}
Summary:        Masterless puppet module for zoom

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Puppet postfix module for zoom

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/zoom/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/zoom/
touch %{buildroot}/%{_datadir}/puppet/modules/zoom/linuxsupport

%files -n puppet-zoom
%{_datadir}/puppet/modules/zoom

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 1.4-1
- Add autoreconfigure %post script

* Mon Jun 19 2023 Manuel Guijarro <Manuel.Guijarro@cern.ch> 1.3-2
- Using an exec to avoid packaging a selinux module for all EL9 families

* Tue Feb 21 2023 Ben Morrice <ben.morrice@cern.ch> 1.3-1
- CC7 uses a different repo now

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.2-2
- Bump release for disttag change

* Tue Apr 19 2022 Ben Morrice <ben.morrice@cern.ch> - 1.2-1
- Configure selinuxuser_execmod on CS9

* Wed Nov 17 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-1
- remove gpg check, as zoom is no longer signing rpms

* Thu Oct 28 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- bugfix packaging

* Mon Sep 27 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
-Initial release

