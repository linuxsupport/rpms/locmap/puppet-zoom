class zoom {

  if $facts['os']['distro']['release']['major'] == '7' {
    $zoomrepo = 'https://linuxsoft.cern.ch/mirror/cern.zoom.us/client/el7/'
  } else {
    $zoomrepo = 'https://linuxsoft.cern.ch/mirror/cern.zoom.us/client/latest/'
  }
  yumrepo { 'zoom':
    ensure   => present,
    descr    => 'CERN zoom client',
    baseurl  => $zoomrepo,
    gpgcheck => false,
  }
  package {'zoom':
    ensure  => present,
    require => Yumrepo['zoom'],
  }

  # LOS-907 / INC3508363 : selinuxuser_execmod is default on in el7,el8 - but not on el9
  # Zoom needs it enabled
  if $facts['os']['distro']['release']['major'] == '9' {
    # Using an exec to avoid packaging a selinux module
    exec {'selinux_allow_selinuxuser_execmod':
      command => '/usr/sbin/setsebool -P selinuxuser_execmod on',
      onlyif  => '/usr/sbin/getsebool selinuxuser_execmod | /usr/bin/grep off'
    }
  }
}
